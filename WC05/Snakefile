configfile: "config.yaml"
include: "samples.smk"

from os.path import join

FASTQ_DIR = 'data/'

rule all:
    input:
         "out.html"

rule bwa_map:
    input:
        FASTQ_DIR + 'genome.fa',
        FASTQ_DIR + 'samples/{sample}.fastq'
    output:
        "mapped_reads/{sample}.bam"
    benchmark:
        "benchmarks/{sample}.bwa.benchmark.txt"
    log:
        "logs/bwa_mem/{sample}.log"
    threads: 8
    shell:
        "(bwa mem  -t {threads} {input} | "
        "samtools view -Sb - > {output}) 2> {log}"
        
        
        
rule samtools_index:
    input:
        "sorted_reads/{sample}.bam"
    output:
        "sorted_reads/{sample}.bam.bai"
    shell:
        "samtools index {input}"
        
        
rule bcftools_call:
    input:
        fa=FASTQ_DIR + 'genome.fa',
        bam=expand("sorted_reads/{sample}.bam", sample=config["SAMPLES"]),
        bai=expand("sorted_reads/{sample}.bam.bai", sample=config["SAMPLES"])
    output:
        "calls/all.vcf"
    shell:
        "samtools mpileup -g -f {input.fa} {input.bam} | "
        "bcftools call -mv - > {output}"
        
rule report:
    input:
        "calls/all.vcf"
    output:
        "out.html"
    run:
        from snakemake.utils import report
        with open(input[0]) as f:
            n_calls = sum(1 for line in f if not line.startswith("#"))

        report("""
        An example workflow
        ===================================

        Reads were mapped to the Yeas reference genome 
        and variants were called jointly with
        SAMtools/BCFtools.

        This resulted in {n_calls} variants (see Table T1_).
        """, output[0], metadata="Author: Mr Pipeline", T1=input[0])