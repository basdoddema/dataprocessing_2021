"""This script adds annotation to Scanpy samples"""
import scanpy as sc
import sys


# sample = 'Batch2-Day17'
samples = ['Batch1-Day3', 'Batch1-Day7', 'Batch1-Day10', 'Batch1-Day14', 'Batch2-Day3', 'Batch2-Day7',
           'Batch2-Day10', 'Batch2-Day14', 'Batch2-Day17', 'Batch3-Day3', 'Batch3-Day7', 'Batch3-Day10',
           'Batch3-Day14', 'Batch3-Day17']

# sample = "Batch1-Day3"
results_file = sys.argv[1]
output = sys.argv[2]

for item in samples:
    if item in results_file:
        sample = item

# Read file
# results_file = "/media/martijn/Elements/Bas/Samples/" + sample + "/write/QC_output.h5ad"
adata = sc.read(results_file)

# cluster to cell type assignment
cluster_to_celltype = {
    "Batch1-Day3": {
        0: "Secretory", 1: "Basal", 2: "Basal_to_Secretory", 3: "Secretory", 4: "Basal_to_Secretory",
        5: "Proliferating", 6: "Basal", 7: "Basal_to_Secretory", 8: "Proliferating"},
    "Batch1-Day7": {
        0: "Basal", 1: "Basal_to_Secretory", 2: "Secretory", 3: 'Proliferating', 4: "Basal_to_Secretory"},
    "Batch1-Day10": {
        0: "Basal", 1: "Secretory", 2: 'Secretory', 3: "Basal_to_Secretory", 4: "Proliferating", 5: "Basal"},
    "Batch1-Day14": {
        0: "Secretory", 1: "Basal", 2: 'Basal_to_Secretory', 3: "Secretory", 4: "Basal", 5: "Proliferating"},
    "Batch2-Day3": {
        0: "Secretory", 1: "Secretory", 2: "Basal", 3: "Basal", 4: "Basal_to_Secretory",
        5: "Proliferating", 6: "Basal", 7: "Basal_to_Secretory"},
    "Batch2-Day7": {
        0: "Basal", 1: "Basal", 2: "Secretory", 3: 'Proliferating'},
    "Batch2-Day10": {
        0: "Secretory", 1: "Basal", 2: 'Basal_to_Secretory', 3: "Basal_to_Secretory", 4: "Proliferating"},
    "Batch2-Day14": {
        0: "Basal", 1: "Basal_to_Secretory", 2: 'Secretory', 3: "Basal", 4: "Basal_to_Secretory", 5: "Basal",
        6: "Secretory", 7: "Proliferating", 8: 'Basal_to_Secretory', 9: "Basal", 10: "Basal"},
    "Batch2-Day17": {
        0: "Basal", 1: "Secretory", 2: 'Secretory', 3: "Secretory", 4: "Secretory", 5: "Basal",
        6: "Basal", 7: "Basal_to_Secretory", 8: 'Proliferating', 9: "Basal", 10: "Basal_to_Secretory"},
    "Batch3-Day3": {
        0: "Basal", 1: "Basal_to_Proliferating", 2: "Secretory", 3: "Basal_to_Proliferating", 4: "Secretory",
        5: "Basal"},
    "Batch3-Day7": {
        0: "Basal", 1: "Basal_to_Secretory", 2: "Proliferating", 3: 'Secretory', 4: "Basal", 5: "Basal"},
    "Batch3-Day10": {
        0: "Basal", 1: "Basal_to_Secretory", 2: 'Basal_to_Secretory', 3: "Secretory", 4: "Proliferating"},
    "Batch3-Day14": {
        0: "Basal", 1: "Secretory", 2: 'Basal_to_Secretory', 3: "Basal", 4: "Proliferating"},
    "Batch3-Day17": {
        0: "Basal", 1: "Secretory", 2: 'Basal_to_Secretory', 3: "Proliferating", 4: "Secretory", 5: "Basal",
        6: "Basal_to_Secretory", 7: "Secretory"}
}

adata.obs['marker_annotation'] = 'NA'

# print(cluster_to_celltype[sample][int(adata.obs["leiden_1"][0])])

if sample in ['Batch1-Day7', 'Batch1-Day10', 'Batch2-Day7', 'Batch2-Day10', 'Batch3-Day3',
              'Batch3-Day7', 'Batch3-Day10', 'Batch3-Day14', 'Batch3-Day17']:
    leiden = 'leiden_0.4'
elif sample in ['Batch1-Day14']:
    leiden = 'leiden_0.6'
else:
    leiden = 'leiden_1'

for i in range(len(adata.obs['marker_annotation'])):
    adata.obs['marker_annotation'][i] = cluster_to_celltype[sample][int(adata.obs[leiden][i])]

# plot results
if sample == 'Batch2-Day10':
    sc.pl.umap(adata,
               color=[leiden, 'marker_annotation', 'KRT5', 'TP63', 'TOP2A', 'MKI67', 'SCGB1A1', 'donor'],
               frameon=False, ncols=5)
elif sample in ['Batch1-Day3', 'Batch1-Day7', 'Batch1-Day10', 'Batch1-Day14', 'Batch2-Day3', 'Batch2-Day7',
                'Batch2-Day14', 'Batch2-Day17', 'Batch3-Day3', 'Batch3-Day7', 'Batch3-Day10',
                'Batch3-Day14', 'Batch3-Day17']:
    sc.pl.umap(adata,
               color=[leiden, 'marker_annotation', 'KRT5', 'TP63', 'TOP2A', 'MKI67', 'SCGB1A1', 'SCGB3A1', 'donor'],
               frameon=False, ncols=5)

print(adata.obs)

# save results
# results_file = "/media/martijn/Elements/Bas/Samples/" + output + "/test/Annotated_data.h5ad"
adata.write(output)

