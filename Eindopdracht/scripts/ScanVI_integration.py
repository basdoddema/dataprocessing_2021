"""Script that integrates scRNAseq samples using scvi/scanvi
3 different integration models were used to integrate"""
import scanpy as sc
import scvi
import anndata as ad
import sys

input1 = sys.argv[1]
input2 = sys.argv[2]
input3 = sys.argv[3]
# input4 = sys.argv[4]
output = sys.argv[4]

# Read in all samples
Batch1_Day3 = sc.read(input1)
Batch1_Day7 = sc.read(input2)
Batch1_Day10 = sc.read(input3)
# Batch1_Day14 = sc.read(input4)

# Remove more cells for samples that don't integrate well
Batch1_Day7 = Batch1_Day7[(Batch1_Day7.obs["total_counts"] > 1000) & (Batch1_Day7.obs['n_genes_by_counts'] > 500)]

# Create dictionary holding samples
batches = {"Batch1-Day3": Batch1_Day3, "Batch1-Day7": Batch1_Day7, "Batch1-Day10": Batch1_Day10}

# Add sample ID, batch number and extra covariate to all samples
for batch_day in batches.keys():
    batches[batch_day].obs['sample_id'] = [batch_day] * len(batches[batch_day].obs)
    batch = batch_day.split("-")
    batches[batch_day].obs['batch_nr'] = [batch[0]] * len(batches[batch_day].obs)
    # add extra covariate to the annotation data object containing batch day if sample integrates bad
    if batch_day in ["Batch2-Day10", "Batch2-Day3", "Batch1-Day7", "Batch2-Day14"]:
        batches[batch_day].obs['extra_covariate'] = batch_day
        # batches[batch_day].obs['extra_covariate'] = "True"
    else:
        batches[batch_day].obs['extra_covariate'] = "False"
    batches[batch_day].obs['day_nr'] = [batch[1]] * len(batches[batch_day].obs)


adata = ad.AnnData.concatenate(Batch1_Day3, Batch1_Day7, Batch1_Day10)

# for i in range (0, 14):
#     adata.var["mt-"+str(i)] = adata.var["mt-0"].fillna(False)
#     adata.var["highly_variable-"+str(i)] = adata.var["highly_variable-0"].fillna(False)

# Change intermediate cell types to Unknown
adata.obs['marker_annotation'] = adata.obs['marker_annotation'].replace(['Basal_to_Secretory'], 'Unknown')
adata.obs['marker_annotation'] = adata.obs['marker_annotation'].replace(['Basal_to_Proliferating'], 'Unknown')

# set raw data
adata.raw = adata

sc.pp.highly_variable_genes(
    adata,
    n_top_genes=3000,
    batch_key="batch_nr"
)

print(adata)


# setup data for scvi model
# model using Sample ID
# scvi.model.SCVI.setup_anndata(adata, batch_key="sample_id", layer="counts")
# model using batch number
# scvi.model.SCVI.setup_anndata(adata, batch_key="batch_nr", layer="counts")
# model using batch number + extra covariate
scvi.model.SCVI.setup_anndata(adata, batch_key="batch_nr", layer="counts",
                              categorical_covariate_keys=["extra_covariate"])

scvi_model = scvi.model.SCVI(adata, n_latent=10, n_layers=2, gene_likelihood="nb")

# Train model with 200 epochs
scvi_model.train(5)

adata.obsm["X_scVI"] = scvi_model.get_latent_representation(adata)

# compute neighbors and cluster using leiden clustering
sc.pp.neighbors(adata, use_rep="X_scVI")
sc.tl.leiden(adata, key_added="scvi_leiden")
sc.tl.umap(adata)
sc.pl.umap(adata, color=["batch_nr", "scvi_leiden"])

# setup data for scanvi model
# model using Sample ID
# scvi.model.SCANVI.setup_anndata(adata, layer="counts", batch_key="sample_id", labels_key="marker_annotation")
# model using batch number
# scvi.model.SCANVI.setup_anndata(adata, layer="counts", batch_key="batch_nr", labels_key="marker_annotation")
# model using batch number + extra covariate
scvi.model.SCANVI.setup_anndata(adata, layer="counts", batch_key="batch_nr", labels_key="marker_annotation",
                                categorical_covariate_keys=["extra_covariate"])
vae = scvi.model.SCANVI.from_scvi_model(scvi_model, "Unknown", adata=adata)

print(vae)

# Train scanvi model using 20 epochs
vae.train(max_epochs=5, n_samples_per_label=100)
adata.obsm["X_scANVI"] = vae.get_latent_representation()
adata.obs["scanvi_prediction"] = vae.predict()

# neighbourhood graph & UMAP embedding
sc.pp.neighbors(adata, use_rep="X_scANVI")
sc.tl.umap(adata)

# inspect result of integration, including reassigned annotation
sc.pl.umap(adata, color=["scanvi_prediction", "sample_id", "batch_nr", 'day_nr', "donor"], frameon=False)

# plot results
sc.pl.umap(adata,
           color=['scanvi_prediction', 'KRT5', 'TP63', 'TOP2A', 'MKI67', 'SCGB1A1'],
           frameon=False, ncols=5)

# save results
# results_file = "write/Integrated_object.h5ad"  # the file that will store the analysis results
adata.write(output)

# save results
# results_file = "write/Integrated_object_new.h5ad"  # the file that will store the analysis results
# adata.write(results_file)

# # save results
# results_file = "write/Integrated_object_hvg.h5ad"  # the file that will store the analysis results
# adata.write(results_file)
