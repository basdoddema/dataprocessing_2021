"""Script to run scVelo and compute the RNA velocity and velocity pseudotime for integrated objects
creates boxplot with pseudotime values per time point"""
import numpy as np
import scanpy as sc
import sys
import os
import pandas as pd
import scvelo as scv
import matplotlib.pyplot as plt


if len(sys.argv) <= 2:
    print("run as follows: python scVelo.py [path to integrated object] [output file name for boxplot with pseudotime]")
    sys.exit()

# get sys parameters
sample = sys.argv[1]
output_file = sys.argv[2]

adata = sc.read(sample)

# add copy of donor
adata.obs["donor_copy"] = adata.obs["donor"]

# # change donors to correct ones for batch1_day7
# adata.obs.loc[(adata.obs["batch_info"] == "Batch1-Day7") & (adata.obs["donor_copy"] == "ARMS095"), "donor"] = "ARMS055"
# adata.obs.loc[(adata.obs["batch_info"] == "Batch1-Day7") & (adata.obs["donor_copy"] == "ARMS055"), "donor"] = "ARMS095"
#
# # change donors to correct ones for batch1_day3
# adata.obs.loc[(adata.obs["batch_info"] == "Batch1-Day3") & (adata.obs["donor_copy"] == "ARMS055"), "donor"] = "ARMS070"
# adata.obs.loc[(adata.obs["batch_info"] == "Batch1-Day3") & (adata.obs["donor_copy"] == "ARMS070"), "donor"] = "ARMS094"
# adata.obs.loc[(adata.obs["batch_info"] == "Batch1-Day3") & (adata.obs["donor_copy"] == "ARMS094"), "donor"] = "ARMS095"
# adata.obs.loc[(adata.obs["batch_info"] == "Batch1-Day3") & (adata.obs["donor_copy"] == "ARMS095"), "donor"] = "ARMS055"
#
# # change donors to correct ones for batch1_day14
# adata.obs.loc[(adata.obs["batch_info"] == "Batch1-Day14") & (adata.obs["donor_copy"] == "ARMS095"), "donor"] = "ARMS094"
# adata.obs.loc[(adata.obs["batch_info"] == "Batch1-Day14") & (adata.obs["donor_copy"] == "ARMS094"), "donor"] = "ARMS095"

adata = adata[adata.obs["scanvi_prediction"].isin(["Basal", "Secretory"])]

# sc.pl.umap(adata, ncols=2,
#            color=['total_counts', 'n_genes_by_counts', 'pct_counts_mt'])

# Add classification to donors
adata.obs['classification'] = adata.obs['donor'].replace(['ARMS015', 'ARMS019', 'ARMS049', 'ARMS069',
                                                          'ARMS070', 'ARMS094'], 'Asthma')
adata.obs['classification'] = adata.obs['classification'].replace(['ARMS022', 'ARMS046', 'ARMS054', 'ARMS055',
                                                                   'ARMS095'], 'Healthy')

sc.pl.umap(adata, ncols=2,
           color=["day_nr", "scanvi_prediction"], size=25)

# sc.pl.umap(adata, ncols=2,
#            color=["KRT4", "KRT13", "KRT5"], size=25)

# Recover the velocity using the stochastic model from scVelo
scv.pp.moments(adata)
scv.tl.velocity(adata, mode="stochastic")
scv.tl.velocity_graph(adata, n_jobs=20)

# Create velocity embedding stream
scv.pl.velocity_embedding_stream(adata, basis="umap", color="scanvi_prediction", save=output_file)

# # Compute velocity pseudotime
# scv.tl.velocity_pseudotime(adata)
# scv.pl.scatter(adata, color="velocity_pseudotime", cmap="gnuplot", size=20)
#
# # Subset the pseudotime values to time points
# Day3 = pd.DataFrame({"Day3": adata.obs["velocity_pseudotime"][(adata.obs["day_nr"] == "Day3")]})
# Day7 = pd.DataFrame({"Day7": adata.obs["velocity_pseudotime"][(adata.obs["day_nr"] == "Day7")]})
# Day10 = pd.DataFrame({"Day10": adata.obs["velocity_pseudotime"][(adata.obs["day_nr"] == "Day10")]})
# Day14 = pd.DataFrame({"Day14": adata.obs["velocity_pseudotime"][(adata.obs["day_nr"] == "Day14")]})
# Day17 = pd.DataFrame({"Day17": adata.obs["velocity_pseudotime"][(adata.obs["day_nr"] == "Day17")]})
#
# # Concat into one object
# df = pd.concat([Day3, Day7, Day10, Day14, Day17])
#
# # Create boxplot with pseudotime values per time point
# boxplot = df.boxplot(column=["Day3", "Day7", "Day10", "Day14", "Day17"])
#
# # Set ylabel of boxplot
# boxplot.set_ylabel("pseudotime")
#
# # Set title of boxplot
# plt.title("Pseudotime values vs real-time")
# plt.savefig(output_file)
#
# plt.show()



