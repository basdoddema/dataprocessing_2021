"""This script adds velocyto annotation to Scanpy samples"""
import numpy as np
import scanpy as sc
import sys
import scvelo as scv

# samples = ['Batch1-Day3', 'Batch1-Day7', 'Batch1-Day10', 'Batch1-Day14', 'Batch2-Day3', 'Batch2-Day7',
#            'Batch2-Day10', 'Batch2-Day14', 'Batch2-Day17', 'Batch3-Day3', 'Batch3-Day7', 'Batch3-Day10',
#            'Batch3-Day14', 'Batch3-Day17']



def add_velocyto(sample, loom, output):
    # Read sample
    adata = sc.read(sample)

    # Read in loom file
    loom_file = "/media/martijn/Elements/Bas/Samples/" + sample + "/Annotations/Velocyto.loom"
    velocyto_data = scv.read(loom)

    # Add loom file to scanpy object
    adata = scv.utils.merge(adata, velocyto_data)

    print(adata)

    # save results
    # results_file = "/media/martijn/Elements/Bas/Samples/" + sample + "/write/Annotated_data_velocyto.h5ad"
    adata.write(output)


if __name__ == '__main__':
    sample = sys.argv[1]
    loom = sys.argv[2]
    output = sys.argv[3]

    add_velocyto(sample, loom, output)
