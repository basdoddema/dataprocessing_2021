# a snakemake pipeline for dataprocessing

This repository contains a pipeline to perform some pre-processing steps on scRNAseq data


![](dag.png)

#### seting up the workspace
within a virtual environment install snakemake

```
pip install snakemake
```

#### run the workflow
```
snakemake --snakefile Snakefile --cores all
```

#### results
The workflow outputs a png file of and embedding projected with RNA velocity vectors