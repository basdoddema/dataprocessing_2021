import os
from snakemake.remote.NCBI import RemoteProvider as NCBIRemoteProvider
NCBI = NCBIRemoteProvider(email="b.g.doddema@st.hanze.nl") # email required by NCBI

query = '"SARS-CoV-2"[organism]'
accessions = NCBI.search(query, retmax=4)

input_files = expand("{acc}.fasta", acc=accessions)


rule all:
    input:
        "sizes.fasta"
        
rule download_and_count:
    input:
        NCBI.remote(input_files, db="nuccore", seq_start=5000)

    output:
        "sizes.fasta"
    run:
        shell("wc -c {input} > sizes.fasta")